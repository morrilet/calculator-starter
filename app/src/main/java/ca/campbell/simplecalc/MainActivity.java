package ca.campbell.simplecalc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {
    EditText etNumber1, etNumber2;
    TextView result;
    double num1, num2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // get a handle on the text fields
        etNumber1 = (EditText) findViewById(R.id.num1);
        etNumber2 = (EditText) findViewById(R.id.num2);
        result = (TextView) findViewById(R.id.result);
    }  //onCreate()

    public void addNums(View v) {
        try{
            num1 = Double.parseDouble(etNumber1.getText().toString());
            num2 = Double.parseDouble(etNumber2.getText().toString());
        }
        catch(Exception e){
            result.setText(R.string.error);
            e.printStackTrace();
            return;
        }
        result.setText(Double.toString(num1 + num2));
    }  //addNums()

    public void subtractNums(View v) {
        try{
            num1 = Double.parseDouble(etNumber1.getText().toString());
            num2 = Double.parseDouble(etNumber2.getText().toString());
        }
        catch(Exception e){
            result.setText(R.string.error);
            e.printStackTrace();
            return;
        }
        result.setText(Double.toString(num1 - num2));
    }

    public void multiplyNums(View v) {
        try{
            num1 = Double.parseDouble(etNumber1.getText().toString());
            num2 = Double.parseDouble(etNumber2.getText().toString());
        }
        catch(Exception e){
            result.setText(R.string.error);
            e.printStackTrace();
            return;
        }
        result.setText(Double.toString(num1 * num2));
    }

    public void divideNums(View v) {
        try{
            num1 = Double.parseDouble(etNumber1.getText().toString());
            num2 = Double.parseDouble(etNumber2.getText().toString());

            if(num2 == 0){
                result.setText(R.string.error);
                return;
            }
        }
        catch(Exception e){
            result.setText(R.string.error);
            e.printStackTrace();
            return;
        }
        result.setText(Double.toString(num1 / num2));
    }

    public void clear(View v){
        etNumber1.setText("");
        etNumber2.setText("");
        result.setText("");
    }
}